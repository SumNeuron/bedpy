# make latest version
python setup.py sdist bdist_wheel

# submit latest version
python -m twine upload  dist/*<version>*
python -m twine upload --repository-url https://upload.pypi.org/legacy/ dist/*<version>*



### About MANIFEST.in
need to include `include_package_data=True` in `setup.py` to have manifest included.
See this [S.O. Post](https://stackoverflow.com/questions/3596979/manifest-in-ignored-on-python-setup-py-install-no-data-files-installed)
and its [answer](https://stackoverflow.com/a/30494253/5623899).
